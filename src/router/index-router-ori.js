import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'

import Colors from '@/views/theme/Colors'
import Typography from '@/views/theme/Typography'

// Views - Base
import Cards from '@/views/base/Cards'
import Switches from '@/views/base/Switches'
import Breadcrumbs from '@/views/base/Breadcrumbs'
import Carousels from '@/views/base/Carousels'
import Collapses from '@/views/base/Collapses'
import Jumbotrons from '@/views/base/Jumbotrons'
import ListGroups from '@/views/base/ListGroups'
import Navs from '@/views/base/Navs'
import Navbars from '@/views/base/Navbars'
import Paginations from '@/views/base/Paginations'
import Popovers from '@/views/base/Popovers'
import ProgressBars from '@/views/base/ProgressBars'
import Tooltips from '@/views/base/Tooltips'

// Views - Buttons
import StandardButtons from '@/views/buttons/StandardButtons'
import ButtonGroups from '@/views/buttons/ButtonGroups'
import Dropdowns from '@/views/buttons/Dropdowns'
import SocialButtons from '@/views/buttons/SocialButtons'

import Charts from '@/views/Charts'

// Views - Editors
import TextEditors from '@/views/editors/TextEditors'
import CodeEditors from '@/views/editors/CodeEditors'

// Views - CMS
// import CmsForms from '@/views/cms/CmsForms'

// Views - ChangeSelfPassword
import ChangeSelfPassword from '@/views/usersetting/ChangeSelfPassword'

// Views - Forms
import BasicForms from '@/views/forms/BasicForms'
import AdvancedForms from '@/views/forms/AdvancedForms'
import EditForms from '@/views/forms/EditForms'
import ViewForms from '@/views/forms/ViewForms'
import DeleteUser from '@/views/forms/DeleteUser'

// Views - GettingStarted
// import BasicForms from '@/views/forms/BasicForms'
// import AdvancedForms from '@/views/forms/AdvancedForms'
// import EditForm from '@/views/gettingStarted/EditForms'
// import ViewForms from '@/views/forms/ViewForms'
// import DeleteUser from '@/views/forms/DeleteUser'
import Email from '@/views//gettingStarted/email'
import Message from '@/views//gettingStarted/message'
import Bulletin from '@/views//gettingStarted/bulletin'
import Bulletins from '@/views//gettingStarted/bulletins'
import Compose from '@/views//gettingStarted/compose'
import Layanan from '@/views//gettingStarted/layanan'
import Upgrade from '@/views//gettingStarted/upgrade'
import View from '@/views//gettingStarted/view'
import Agent from '@/views//gettingStarted/agent'
import Profile from '@/views//gettingStarted/profile'
import Rekeningku from '@/views//gettingStarted/rekeningku'
import Mulai from '@/views//gettingStarted/mulai'

// Views - Icons
import Flags from '@/views/icons/Flags'
import FontAwesome from '@/views/icons/FontAwesome'
import SimpleLineIcons from '@/views/icons/SimpleLineIcons'

// Views - Notifications
import Alerts from '@/views/notifications/Alerts'
import Badges from '@/views/notifications/Badges'
import Modals from '@/views/notifications/Modals'
import Toastr from '@/views/notifications/Toastr'

import Tables from '@/views/tables/Tables'
import CMSTables from '@/views/cmstables/Tables'

import Widgets from '@/views/Widgets'

// Views - Pages
import Page404 from '@/views/pages/Page404'
import Page500 from '@/views/pages/Page500'
import Login from '@/views/pages/Login'
import Register from '@/views/pages/Register'
import Otp from '@/views/pages/Otp'
import OtpEntry from '@/views/pages/OtpEntry'
import OtpLogin from '@/views/pages/OtpLogin'

Vue.use(Router)

export default new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/pages/login',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'theme',
          redirect: '/theme/colors',
          name: 'Theme',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'colors',
              name: 'Colors',
              component: Colors
            },
            {
              path: 'typography',
              name: 'Typography',
              component: Typography
            }
          ]
        },
        {
          path: 'base',
          redirect: '/base/cards',
          name: 'Base',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'breadcrumbs',
              name: 'Breadcrumbs',
              component: Breadcrumbs
            },
            {
              path: 'cards',
              name: 'Cards',
              component: Cards
            },
            {
              path: 'carousels',
              name: 'Carousels',
              component: Carousels
            },
            {
              path: 'collapses',
              name: 'Collapses',
              component: Collapses
            },
            {
              path: 'jumbotrons',
              name: 'Jumbotrons',
              component: Jumbotrons
            },
            {
              path: 'list-groups',
              name: 'List Groups',
              component: ListGroups
            },
            {
              path: 'navs',
              name: 'Navs',
              component: Navs
            },
            {
              path: 'navbars',
              name: 'Navbars',
              component: Navbars
            },
            {
              path: 'paginations',
              name: 'Paginations',
              component: Paginations
            },
            {
              path: 'popovers',
              name: 'Popovers',
              component: Popovers
            },
            {
              path: 'progress-bars',
              name: 'Progress Bars',
              component: ProgressBars
            },

            {
              path: 'switches',
              name: 'Switches',
              component: Switches
            },
            {
              path: 'tooltips',
              name: 'Tooltips',
              component: Tooltips
            }
          ]
        },
        {
          path: 'buttons',
          redirect: '/buttons/buttons',
          name: 'Buttons',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'standard-buttons',
              name: 'Standard Buttons',
              component: StandardButtons
            },
            {
              path: 'button-groups',
              name: 'Button Groups',
              component: ButtonGroups
            },
            {
              path: 'dropdowns',
              name: 'Dropdowns',
              component: Dropdowns
            },
            {
              path: 'social-buttons',
              name: 'Social Buttons',
              component: SocialButtons
            }
          ]
        },
        {
          path: 'charts',
          name: 'Charts',
          component: Charts
        },
        {
          path: 'editors',
          redirect: '/editors/text-editors',
          name: 'Editors',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'text-editors',
              name: 'Text Editors',
              component: TextEditors
            },
            {
              path: 'code-editors',
              name: 'Code Editors',
              component: CodeEditors
            }
          ]
        },
        {
          path: 'forms',
          redirect: '/forms/basic-forms',
          name: 'Forms',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'basic-forms',
              name: 'Basic Forms',
              component: BasicForms
            },
            {
              path: 'advanced-forms',
              name: 'Advanced Forms',
              component: AdvancedForms
            },
            {
              path: 'edit-forms',
              name: 'Edit Forms',
              component: EditForms
            },
            {
              path: 'view-forms',
              name: 'View Forms',
              component: ViewForms
            }
          ]
        },
        {
          path: 'gettingStarted',
          redirect: '/gettingStarted/email',
          name: 'Getting Started',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'basic-forms',
              name: 'Create Company Name',
              component: BasicForms
            },
            {
              path: 'advanced-forms',
              name: 'Advanced Forms',
              component: AdvancedForms
            },
            {
              path: 'edit-forms',
              name: 'Edit Forms',
              component: EditForms
            },
            {
              path: 'view-forms',
              name: 'View Forms',
              component: ViewForms
            },
            {
              path: 'email',
              name: 'Email',
              component: Email
            },
            {
              path: 'message',
              name: 'Message',
              component: Message
            },
            {
              path: 'bulletin',
              name: 'Bulletin',
              component: Bulletin
            },
            {
              path: 'bulletins',
              name: 'Bulletins',
              component: Bulletins
            },
            {
              path: 'compose',
              name: 'Compose',
              component: Compose
            },
            {
              path: 'layanan',
              name: 'Layanan',
              component: Layanan
            },
            {
              path: 'upgrade',
              name: 'Upgrade',
              component: Upgrade
            },
            {
              path: 'agent',
              name: 'Agent',
              component: Agent
            },
            {
              path: 'view',
              name: 'View',
              component: View
            },
            {
              path: 'profile',
              name: 'Profile',
              component: Profile
            },
            {
              path: 'rekeningku',
              name: 'Rekeningku',
              component: Rekeningku
            },
            {
              path: 'mulai',
              name: 'Mulai',
              component: Mulai
            }
          ]
        },
        {
          path: 'Profile',
          redirect: '/profile/basic-forms',
          name: 'Profile',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'basic-forms',
              name: 'Create Company Name',
              component: BasicForms
            },
            {
              path: 'advanced-forms',
              name: 'Advanced Forms',
              component: AdvancedForms
            },
            {
              path: 'edit-forms',
              name: 'Edit Forms',
              component: EditForms
            },
            {
              path: 'view-forms',
              name: 'View Forms',
              component: ViewForms
            }
          ]
        },
        {
          path: 'Dashboard',
          redirect: '/dashboard-admin/basic-forms',
          name: 'Dashboard ',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'basic-forms',
              name: 'Create Company Name',
              component: BasicForms
            },
            {
              path: 'advanced-forms',
              name: 'Advanced Forms',
              component: AdvancedForms
            },
            {
              path: 'edit-forms',
              name: 'Edit Forms',
              component: EditForms
            },
            {
              path: 'view-forms',
              name: 'View Forms',
              component: ViewForms
            },
            {
              path: 'ubah-forms',
              name: 'Ubah Forms',
              component: EditForms
            },
            {
              path: 'lihat-forms',
              name: 'Lihat Forms',
              component: ViewForms
            }
          ]
        },
        {
          path: 'Purchase',
          redirect: '/dashboard-admin/basic-forms',
          name: 'Dashboard ',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'basic-forms',
              name: 'Create Company Name',
              component: BasicForms
            },
            {
              path: 'advanced-forms',
              name: 'Advanced Forms',
              component: AdvancedForms
            },
            {
              path: 'edit-forms',
              name: 'Edit Forms',
              component: EditForms
            },
            {
              path: 'view-forms',
              name: 'View Forms',
              component: ViewForms
            },
            {
              path: 'ubah-forms',
              name: 'Ubah Forms',
              component: EditForms
            },
            {
              path: 'lihat-forms',
              name: 'Lihat Forms',
              component: ViewForms
            }
          ]
        },
        {
          path: '/edit-forms/:id',
          name: 'Edit Forms',
          component: EditForms
        },
        {
          path: '/view-forms/:id',
          name: 'View Forms',
          component: ViewForms
        },
        {
          path: '/delete-user/:id',
          name: 'Delete User',
          component: DeleteUser
        },
        // {
        //   path: 'cms',
        //   redirect: '/cms/basic-cms',
        //   name: 'cms',
        //   component: {
        //     render (c) { return c('router-view') }
        //   },
        //   children: [
        //     {
        //       path: 'cms-forms',
        //       name: 'Cms Forms',
        //       component: CmsForms
        //     },
        //     {
        //       path: 'basic-forms',
        //       name: 'Basic Forms',
        //       component: BasicForms
        //     },
        //     {
        //       path: 'advanced-forms',
        //       name: 'Advanced Forms',
        //       component: AdvancedForms
        //     }
        //   ]
        // },
        {
          path: 'usersetting',
          redirect: '/usersetting/change-self-password',
          name: 'User Setting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'change-self-password',
              name: 'Change Self Password',
              component: ChangeSelfPassword
            }
          ]
        },
        {
          path: 'icons',
          redirect: '/icons/font-awesome',
          name: 'Icons',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'flags',
              name: 'Flags',
              component: Flags
            },
            {
              path: 'font-awesome',
              name: 'Font Awesome',
              component: FontAwesome
            },
            {
              path: 'simple-line-icons',
              name: 'Simple Line Icons',
              component: SimpleLineIcons
            }
          ]
        },
        {
          path: 'notifications',
          redirect: '/notifications/alerts',
          name: 'Notifications',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'alerts',
              name: 'Alerts',
              component: Alerts
            },
            {
              path: 'badges',
              name: 'Badges',
              component: Badges
            },
            {
              path: 'modals',
              name: 'Modals',
              component: Modals
            },
            {
              path: 'toastr',
              name: 'Toastr',
              component: Toastr
            }
          ]
        },
        {
          path: 'tables',
          name: 'Tables',
          component: Tables
        },
        {
          path: 'cmstables',
          name: 'cmstables',
          component: CMSTables
        },
        {
          path: 'widgets',
          name: 'Widgets',
          component: Widgets
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        },
        {
          path: 'otp',
          name: 'Otp',
          component: Otp
        },
        {
          path: 'otpEntry',
          name: 'OtpEntry',
          component: OtpEntry
        },
        {
          path: 'otpLogin',
          name: 'OtpLogin',
          component: OtpLogin
        }
      ]
    }
  ]
})
