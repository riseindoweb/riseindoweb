import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// import Dashboard from '@/views/Dashboard'
import Page404 from '@/views/pages/Page404'
import Page500 from '@/views/pages/Page500'
import Index from '@/views/pages/index'
import caraPemodalan from '@/views/pages/CaraPemodalan'
import TentangKami from '@/views/pages/tentangKami'
import PencapaianKami from '@/views/pages/PencapaianKami'
import Login from '@/views/pages/Login'
import Register from '@/views/pages/Register'
import Resetpassword from '@/views/pages/ResetPassword'
import Forgotpassword from '@/views/pages/ForgotPassword'
import Proyekpemodalan from '@/views/pages/ProyekPemodalan'
import Skemapemodalan from '@/views/pages/SkemaPemodalan'
import detailProyek from '@/views/pages/detailProyek'
import Faq from '@/views/pages/FAQ'
import privacy from '@/views/pages/privacy'
import Dashboard from '@/views/user/Dashboard'

Vue.use(Router)

export default new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '',
      component: Full,
      children: [
        { path: '', name: 'Index', component: Index },
        { path: 'detailproyek/:id', name: 'detailproyek', component: detailProyek },
        { path: 'cara-pemodalan', name: 'carapemodalan', component: caraPemodalan },
        { path: 'tentang-kami', name: 'tentangKami', component: TentangKami },
        { path: 'skema-pemodalan', name: 'Skemapemodalan', component: Skemapemodalan },
        { path: 'pencapaian-kami', name: 'PencapaianKami', component: PencapaianKami },
        { path: 'proyek-pemodalan', name: 'ProyekPemodalan', component: Proyekpemodalan },
        { path: 'faq', name: 'Faq', component: Faq },
        { path: 'privacy', name: 'privacy', component: privacy }
      ]
    },
    { path: '/login', name: 'Login', component: Login },
    { path: '/register', name: 'Register', component: Register },
    { path: '/reset-password', name: 'Resetpassword', component: Resetpassword },
    { path: '/forgot-password', name: 'Forgotpassword', component: Forgotpassword },
    {
      path: '/user/',
      redirect: 'user',
      component: Full,
      children: [
        { path: '', name: 'Dashboard', component: Dashboard }
      ]
    },
    // {
    //   path: '/kotakmasuk',
    //   redirect: '/kotakmasuk',
    //   component: Full,
    //   children: [
    //     { path: 'email', name: 'Email', component: Email },
    //     { path: 'detailEmail', name: 'detailEmail',  component: detailEmail},
    //     { path: 'Bulletin', name: 'Bulletin',  component: Bulletin },
    //     { path: 'Message', name: 'Message', component: Message }
    //   ]
    // },
    // {
    //   path: '/stok',
    //   redirect: '/stok',
    //   component: Full,
    //   children: [
    //     { path: 'laporanbarangmasuk', name: 'laporanbarangmasuk', component: laporanbarangmasuk },
    //     { path: 'laporanbarangkeluar', name: 'laporanbarangkeluar', component: laporanbarangkeluar }
    //   ]
    // },
    // {
    //   path: '/barang',
    //   redirect: '/barang',
    //   component: Full,
    //   children: [
    //     { path: 'add', name: 'addbarang', component: barangAdd },
    //     { path: 'list', name: 'baranglist', component: barangList },
    //     { path: 'edit/:id', name: 'barangedit', component: barangEdit },
    //     { path: 'view/:id', name: 'barangview', component: barangView },
    //     { path: 'dashboard_caleg', name: 'dashboard_caleg', component: DashboardCaleg }
    //   ]
    // },
    // {
    //   path: '/barangmasuk',
    //   redirect: '/barangmasuk',
    //   component: Full,
    //   children: [
    //     { path: 'add', name: 'addbarangmasuk', component: barangmasukAdd },
    //     { path: 'list', name: 'barangmasuklist', component: barangmasukList },
    //     { path: 'edit/:id', name: 'barangmasukedit', component: barangmasukEdit },
    //     { path: 'view/:id', name: 'barangmasukview', component: barangmasukView }
    //   ]
    // },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
      ]
    },
    {
      path: '*',
      name: 'Page404',
      component: Page404
    },
  ]
})
